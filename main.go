package main

import (
	"os"

	"gitlab.com/illiapopov/my-blob-service-svc/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}

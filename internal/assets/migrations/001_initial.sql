-- +migrate Up
CREATE table blob (
    id bigserial PRIMARY KEY,
    value jsonb not null
);
-- +migrate Down
drop table blob;

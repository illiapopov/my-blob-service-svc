package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/data/pg"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/service/handlers"
)

func (s *service) router() chi.Router {
	info, err := s.config.HorizonConnector().State()
	if err != nil {
		panic(errors.Wrap(err, "failed to get horizon info"))
	}

	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobQ(pg.NewBlobQ(s.db)),
			//handlers.CtxBlobQ(horizon.NewBlobQBc(s.config.HorizonConnector())),
			handlers.CtxHorizon(s.config.HorizonConnector()),
			handlers.CtxHorizonInfo(info),
		),
	)
	r.Route("/integrations/my-blob-service-svc", func(r chi.Router) {
		r.Post("/Blobs", handlers.CreateBlob)
		r.Get("/Blobs", handlers.GetBlobList)
		r.Route("/Blobs/{id}", func(r chi.Router) {
			r.Get("/", handlers.GetBlob)
			r.Delete("/", handlers.DeleteBlob)
			r.Patch("/", handlers.UpdateBlob)
		})
	})

	return r
}

package handlers

import (
	"context"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/data"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/horizon"
	regources "gitlab.com/tokend/regources/generated"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	blobQCtxKey
	horizonCtxKey
	horizonInfoCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func BlobQ(r *http.Request) data.BlobQ {
	return r.Context().Value(blobQCtxKey).(data.BlobQ).New()
}

func CtxBlobQ(entry data.BlobQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, blobQCtxKey, entry)
	}
}

func CtxHorizon(horizon *horizon.Connector) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, horizonCtxKey, horizon)
	}
}

func Horizon(r *http.Request) *horizon.Connector {
	return r.Context().Value(horizonCtxKey).(*horizon.Connector)
}

func CtxHorizonInfo(entry *regources.HorizonState) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, horizonInfoCtxKey, entry)
	}
}

func HorizonInfo(r *http.Request) *regources.HorizonState {
	return r.Context().Value(horizonInfoCtxKey).(*regources.HorizonState)
}

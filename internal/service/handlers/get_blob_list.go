package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/data"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/service/helpers"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/service/requests"
	"log"
	"net/http"
)

func GetBlobList(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetBlobsListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	log.Println(request.OffsetPageParams)

	blobQ := BlobQ(r)
	applyFilters(blobQ, request)
	result, err := blobQ.Select()
	if err != nil {
		Log(r).WithError(err).Error("failed to get blobs")
		ape.Render(w, problems.InternalError())
		return
	}

	result.Links = helpers.GetOffsetLinks(r, request.OffsetPageParams)

	ape.Render(w, result)
}

func applyFilters(q data.BlobQ, request requests.GetBlobsListRequest) {
	q.Page(request.OffsetPageParams)
}

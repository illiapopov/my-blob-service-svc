package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/data"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/service/requests"
	"gitlab.com/illiapopov/my-blob-service-svc/resources"
	"net/http"
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	var resultBlob *data.Blob

	blob := data.Blob{
		Value: request.Data.Attributes.Value,
	}

	resultBlob, err = BlobQ(r).Insert(blob)
	if err != nil {
		Log(r).WithError(err).Error("failed to insert blob")
		ape.Render(w, problems.InternalError())
		return
	}

	ape.Render(w, resources.BlobResponse{
		Data: NewBlobModel(*resultBlob),
	})

}

func NewBlobModel(blob data.Blob) resources.Blob {
	result := resources.Blob{
		Key: resources.NewKeyInt64(blob.ID, "Blob"),
		Attributes: resources.BlobAttributes{
			Value: blob.Value,
		},
	}

	return result
}

package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/service/requests"
	"net/http"
)

func DeleteBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewDeleteBlobRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	err = BlobQ(r).Delete(uint64(request.BlobID))

	if err != nil {
		Log(r).WithError(err).Error("failed to delete blob")
		ape.Render(w, problems.NotFound())
		return
	} else {
		ape.Render(w, http.StatusOK)
	}

}

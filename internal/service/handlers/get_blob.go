package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/service/requests"
	"gitlab.com/illiapopov/my-blob-service-svc/resources"
	"net/http"
)

func GetBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetBlobRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blob, err := BlobQ(r).GetById(uint64(request.BlobID))
	if err != nil {
		Log(r).WithError(err).Error("failed to get blob")
		ape.Render(w, problems.InternalError())
		return
	}
	if blob == nil {
		ape.Render(w, problems.NotFound())
		return
	}

	ape.Render(w, resources.BlobResponse{
		Data: NewBlobModel(*blob),
	})
}

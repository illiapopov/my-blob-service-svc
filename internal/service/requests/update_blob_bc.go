package requests

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
)

type UpdateBlobRequest struct {
	BlobID int64           `url:"-"`
	Value  json.RawMessage `json:"value"`
}

func NewUpdateBlobRequest(r *http.Request) (UpdateBlobRequest, error) {
	request := UpdateBlobRequest{}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}
	request.BlobID = cast.ToInt64(chi.URLParam(r, "id"))

	return request, nil
}

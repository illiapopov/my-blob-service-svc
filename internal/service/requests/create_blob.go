package requests

import (
	"encoding/json"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/illiapopov/my-blob-service-svc/resources"
	"net/http"
)

type CreateBlobRequest struct {
	Data resources.Blob
}

func NewCreateBlobRequest(r *http.Request) (CreateBlobRequest, error) {
	var request CreateBlobRequest

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}

	//return request, validate(request)
	return request, nil
}

//func validate(b resources.Blob) error {
//	return mergeErrors(validation.Errors{
//		"/attributes/login/": validation.Validate(&b.Attributes.Login, validation.Required,
//			validation.Length(1, 20)),
//		"/attributes/balance/": validation.Validate(&b.Attributes.Balance, validation.Required,
//			validation.Min(0)),
//	}).Filter()
//}

func mergeErrors(validationErrors ...validation.Errors) validation.Errors {
	result := make(validation.Errors)
	for _, errs := range validationErrors {
		for key, err := range errs {
			result[key] = err
		}
	}
	return result
}

package config

import (
	"gitlab.com/illiapopov/my-blob-service-svc/internal/horizon"
	"net/http"
	"net/url"

	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	abstract "gitlab.com/tokend/connectors/horizon"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type HorizonConfig struct {
	Endpoint           *url.URL `fig:"endpoint"`
	IdentitiesEndpoint *url.URL `fig:"identities_endpoint"`
}

func (c *config) HorizonConnector() *horizon.Connector {
	return c.horizonConnector.Do(func() interface{} {
		var config HorizonConfig

		err := figure.
			Out(&config).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "horizon")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out horizon"))
		}

		signer := c.Keyer.Keys().Signer

		cli := signed.NewClient(http.DefaultClient, config.Endpoint)
		if signer != nil {
			cli = cli.WithSigner(signer)
		}
		connector := abstract.NewConnector(cli, signer)
		if connector == nil {
			panic("connector is nil")
		}

		apiClient := signed.NewClient(http.DefaultClient, config.IdentitiesEndpoint)
		if signer != nil {
			apiClient = apiClient.WithSigner(signer)
		}

		return horizon.NewConnector(connector, apiClient, signer, c.Keyer.Keys().Source)
	}).(*horizon.Connector)
}

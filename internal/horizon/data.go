package horizon

import (
	connector "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/illiapopov/my-blob-service-svc/resources"
	regources "gitlab.com/tokend/regources/generated"
	"log"
	url2 "net/url"
	"strconv"
)

func (c *Connector) Data(offsetParams pgdb.OffsetPageParams) (*resources.BlobListResponse, error) {
	var result resources.BlobListResponse

	params := url2.Values(make(map[string][]string))
	params.Add("page[limit]", strconv.FormatUint(offsetParams.Limit, 10))

	streamer := connector.NewStreamer(
		c.Connector.Client,
		"/v3/data",
		params,
	)

	if err := streamer.Next(&result); err != nil {
		return &result, errors.Wrap(err, "failed to get data from horizon")
	}
	log.Println(len(result.Data))

	//if len(result.Data) == int(offsetParams.Limit) {
	//	for {
	//		var tempResp resources.BlobListResponse
	//
	//		if err := streamer.Next(&tempResp); err != nil {
	//			return &result, errors.Wrap(err, "failed to get data from horizon")
	//		}
	//
	//		result.Data = append(result.Data, tempResp.Data...)
	//
	//		if len(tempResp.Data) != int(offsetParams.Limit) {
	//			break
	//		}
	//	}
	//}

	return &result, nil
}

func (c *Connector) DataById(id uint64) (*regources.CreateDataOpResponse, error) {
	result := &regources.CreateDataOpResponse{}
	url, _ := url2.Parse("/v3/data/" + strconv.FormatUint(id, 10))

	err := c.Get(url, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get data from horizon")
	}

	return result, nil
}

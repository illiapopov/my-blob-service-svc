package horizon

import (
	"context"
	"encoding/json"
	"errors"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/data"
	"gitlab.com/illiapopov/my-blob-service-svc/resources"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"strconv"
)

func NewBlobQBc(horConnector *Connector) data.BlobQ {
	return &blobQBc{
		Connector: horConnector,
		offsetParams: pgdb.OffsetPageParams{
			Limit:      100,
			Order:      "desc",
			PageNumber: 0,
		},
	}
}

type blobQBc struct {
	*Connector
	offsetParams pgdb.OffsetPageParams
}

func (b *blobQBc) New() data.BlobQ {
	return NewBlobQBc(b.Connector)
}

func (b *blobQBc) GetById(id uint64) (*data.Blob, error) {
	blob, err := b.Connector.DataById(id)
	if err != nil {
		return nil, err
	}
	if blob == nil {
		return nil, nil
	}

	num, err := strconv.ParseInt(blob.Data.ID, 10, 64)
	if err != nil {
		return nil, err
	}

	return &data.Blob{
		ID:    num,
		Value: json.RawMessage(blob.Data.Attributes.Value),
	}, nil
}

func (b *blobQBc) Insert(value data.Blob) (*data.Blob, error) {
	createDataOp := xdrbuild.CreateData{
		Type:  1,
		Value: value.Value,
	}

	txResponse, err := b.Connector.SubmitOps(context.Background(), createDataOp, true)
	if err != nil {
		return nil, errors.New("failed to submit operation")
	}

	var txResult xdr.TransactionResult

	err = xdr.SafeUnmarshalBase64(txResponse.Data.Attributes.ResultXdr, &txResult)
	if err != nil {
		return nil, errors.New("failed to unmarshal tx result")
	}

	blob := data.Blob{
		ID:    int64(txResult.Result.MustResults()[0].Tr.CreateDataResult.MustSuccess().DataId),
		Value: value.Value,
	}

	return &blob, nil
}

func (b *blobQBc) Delete(id uint64) error {
	removeDataOp := xdrbuild.RemoveData{
		ID: id,
	}

	txResponse, err := b.Connector.SubmitOps(context.Background(), removeDataOp, true)
	if err != nil {
		return errors.New("failed to submit operation")
	}

	var txResult xdr.TransactionResult

	err = xdr.SafeUnmarshalBase64(txResponse.Data.Attributes.ResultXdr, &txResult)
	if err != nil {
		return errors.New("failed to unmarshal tx result")
	}

	return nil

}

func (b *blobQBc) Page(pageParams pgdb.OffsetPageParams) data.BlobQ {
	b.offsetParams = pageParams
	return b
}

func (b *blobQBc) Select() (*resources.BlobListResponse, error) {
	response, err := b.Connector.Data(b.offsetParams)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (b *blobQBc) Update(id uint64, value json.RawMessage) error {

	updateDataOp := xdrbuild.UpdateData{
		ID:    id,
		Value: value,
	}

	txResponse, err := b.Connector.SubmitOps(context.Background(), updateDataOp, true)
	if err != nil {
		return errors.New("failed to submit operation")
	}

	var txResult xdr.TransactionResult

	err = xdr.SafeUnmarshalBase64(txResponse.Data.Attributes.ResultXdr, &txResult)
	if err != nil {
		return errors.New("failed to unmarshal tx result")
	}

	return nil
}

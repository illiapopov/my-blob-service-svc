package horizon

import (
	connector "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/horizon"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/keypair"
	regources "gitlab.com/tokend/regources/generated"
	url2 "net/url"
)

type Connector struct {
	*horizon.Connector
	signer     keypair.Full
	source     keypair.Address
	identities *connector.Connector
}

func NewConnector(horConnector *horizon.Connector, identitiesClient *signed.Client, signer keypair.Full, source keypair.Address) *Connector {
	return &Connector{
		Connector:  horConnector,
		signer:     signer,
		source:     source,
		identities: connector.NewConnector(identitiesClient),
	}
}

func (c *Connector) State() (*regources.HorizonState, error) {
	result := &regources.HorizonState{}
	url, _ := url2.Parse("/v3/info")

	err := c.Get(url, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get horizon state")
	}

	return result, nil
}

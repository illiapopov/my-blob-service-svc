package horizon

import (
	"context"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdrbuild"
	regources "gitlab.com/tokend/regources/generated"
)

func (c *Connector) SubmitOps(ctx context.Context, operation xdrbuild.Operation, waitForIngest bool) (*regources.TransactionResponse, error) {
	builder, err := c.TXBuilder()
	if err != nil {
		return nil, errors.Wrap(err, "failed to init builder")
	}

	tx := builder.Transaction(c.source)

	tx = tx.Op(operation)

	envelope, err := tx.Sign(c.signer).Marshal()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create tx")
	}

	return c.SubmitEnvelope(ctx, envelope, waitForIngest)
}

func (c *Connector) SubmitEnvelope(ctx context.Context, envelope string, waitForIngest bool) (*regources.TransactionResponse, error) {
	//url, _ := url.Parse("/v3/transactions")
	response, err := c.Submit(ctx, envelope, true, waitForIngest)
	if err != nil {
		return nil, errors.Wrap(err, "failed to submit tx to horizon")
	}
	return response, nil
}

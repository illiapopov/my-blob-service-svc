package data

import (
	"encoding/json"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/illiapopov/my-blob-service-svc/resources"
)

type BlobQ interface {
	New() BlobQ
	GetById(id uint64) (*Blob, error)
	Insert(data Blob) (*Blob, error)
	Delete(id uint64) error
	Update(id uint64, value json.RawMessage) error
	Page(pageParams pgdb.OffsetPageParams) BlobQ
	Select() (*resources.BlobListResponse, error)
}

type Blob struct {
	ID    int64           `db:"id" structs:"-"`
	Value json.RawMessage `db:"value" structs:"value"`
}

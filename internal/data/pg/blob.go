package pg

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/data"
	"gitlab.com/illiapopov/my-blob-service-svc/internal/service/handlers"
	"gitlab.com/illiapopov/my-blob-service-svc/resources"
)

const blobTableName = "Blob"

func NewBlobQ(db *pgdb.DB) data.BlobQ {
	return &blobQ{
		db:  db.Clone(),
		sql: sq.Select("n.*").From(fmt.Sprintf("%s as n", blobTableName)),
	}
}

type blobQ struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func (b *blobQ) New() data.BlobQ {
	return NewBlobQ(b.db)
}

func (b *blobQ) GetById(id uint64) (*data.Blob, error) {
	var result data.Blob
	err := b.db.Get(&result, sq.Select("*").From(blobTableName).Where(sq.Eq{"id": id}))
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return &result, err
}

func (b *blobQ) Insert(value data.Blob) (*data.Blob, error) {
	clauses := structs.Map(value)
	var result data.Blob
	stmt := sq.Insert(blobTableName).SetMap(clauses).Suffix("returning *")

	err := b.db.Get(&result, stmt)

	return &result, err
}

func (b *blobQ) Delete(id uint64) error {
	stmt := sq.Delete(blobTableName).Where(sq.Eq{"id": id})
	result, err := b.db.ExecWithResult(stmt)
	if err != nil {
		return err
	} else {
		count, err := result.RowsAffected()
		if err != nil {
			return err
		}

		if count == 0 {
			return errors.New("record does not exist")
		}
	}

	return nil
}

func (q *blobQ) Page(pageParams pgdb.OffsetPageParams) data.BlobQ {
	q.sql = pageParams.ApplyTo(q.sql, "id")
	return q
}

func (q *blobQ) Select() (*resources.BlobListResponse, error) {
	var result []data.Blob
	err := q.db.Select(&result, q.sql)

	if err != nil {
		return nil, err
	}

	blobIds := make([]int64, len(result))
	for i, item := range result {
		blobIds[i] = item.ID
	}

	var response []resources.Blob
	for i := range result {
		response = append(response, handlers.NewBlobModel(result[i]))
	}

	return &resources.BlobListResponse{
		Data:     response,
		Included: resources.Included{},
		Links:    nil,
	}, err
}

func (q *blobQ) Update(id uint64, value json.RawMessage) error {
	stmt := sq.Update(blobTableName).Where(sq.Eq{"id": id}).Set("value", value)
	result, err := q.db.ExecWithResult(stmt)
	if err != nil {
		return err
	} else {
		count, err := result.RowsAffected()
		if err != nil {
			return err
		}

		if count == 0 {
			return errors.New("record does not exist")
		}
	}

	return nil
}

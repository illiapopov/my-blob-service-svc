/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Error struct {
	Code    int64  `json:"code"`
	Message string `json:"message"`
}

package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

const assetsUrl = "/v3/assets"

func (c *Connector) Assets(query ...Query) ([]regources.Asset, *regources.Included, error) {
	var response regources.AssetListResponse

	err := c.GetList(assetsUrl, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return []regources.Asset{}, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting assets")
	}

	return response.Data, &response.Included, nil
}

func (c *Connector) AssetByID(id string, query ...Query) (*regources.Asset, *regources.Included, error) {
	var response regources.AssetResponse

	err := c.GetOne(&BasicPath{
		Id:  id,
		Url: assetsUrl,
	}, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return nil, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting asset")
	}

	return &response.Data, &response.Included, nil
}

package horizon

import "fmt"

type Path interface {
	Path() string
}

type BasicPath struct {
	Id  string
	Url string
}

func (p *BasicPath) Path() string {
	return fmt.Sprintf("%s/%s", p.Url, p.Id)
}

type FormatPath struct {
	Id        string
	UrlFormat string
}

func (p *FormatPath) Path() string {
	return fmt.Sprintf(p.UrlFormat, p.Id)
}

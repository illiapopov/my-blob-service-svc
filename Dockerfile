FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/gitlab.com/illiapopov/my-blob-service-svc
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/my-blob-service-svc /go/src/gitlab.com/illiapopov/my-blob-service-svc


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/my-blob-service-svc /usr/local/bin/my-blob-service-svc
COPY config.yaml /
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["my-blob-service-svc"]
